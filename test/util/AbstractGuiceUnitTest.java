package util;

import guice.CustomInjector;
import guice.DevModule;

import com.google.inject.Guice;
import com.google.inject.Injector;

import play.test.UnitTest;

/**
 * A helper class to implement tests by using the Module which is used in the
 * app
 * 
 * @author Michael Ostner
 * 
 */
public abstract class AbstractGuiceUnitTest extends UnitTest {

	final protected Injector injector = new CustomInjector().getInjector();

	/**
	 * 
	 * @param clazz
	 * @return
	 */
	protected <T> T getInstance(final Class<T> clazz) {
		return injector.getInstance(clazz);
	}
}
