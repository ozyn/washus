package util.location;

import java.io.IOException;

import org.junit.Test;

import util.AbstractGuiceUnitTest;

/**
 * Test to check if the used locationFinder implementation seems to work
 * properly
 * 
 * @author Michael Ostner
 * 
 */
public class LocationTest extends AbstractGuiceUnitTest {

	final LocationFinder locationFinder = getInstance(LocationFinder.class);

	@Test
	public void testLocation() {
		Location l = null;
		try {
			l = locationFinder.getLocation("138.246.2.62");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		assertTrue(l.getCity().equalsIgnoreCase("munich"));
		assertTrue(l.getCountry().equalsIgnoreCase("DE"));
	}
}
