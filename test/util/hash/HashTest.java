package util.hash;

import java.nio.charset.Charset;

import org.junit.Test;

import util.AbstractGuiceUnitTest;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * Tests if the used {@link PasswordHasher} works as expected
 * 
 * @author Michael Ostner
 * 
 */
public class HashTest extends AbstractGuiceUnitTest {

	private PasswordHasher hasher = getInstance(PasswordHasher.class);

	@Test
	public void testSha256Hash() {
		final String input = "hello my friend";
		final String expected = "3cfa3c79cb9155debd08edda6fe250abf0d5d2b038ecc40f886c1efb292925e7";

		final String output = hasher.hash(input);
		assertTrue(output.equals(expected));
	}

	@Test
	public void testIfSalted256Hash() {
		HashFunction pureHash = Hashing.sha256();

		String input = "hello my friend";
		String withSalt = "3cfa3c79cb9155debd08edda6fe250abf0d5d2b038ecc40f886c1efb292925e7";

		String pureHashedOutput = pureHash.hashString(input, Charset.forName("UTF-8")).toString();

		assertFalse(pureHashedOutput.equals(withSalt));

	}
}
