package defaultTests;

import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;
import models.*;

/**
 * has no practical use yet. this class is only for showing how to do an applicationTest case
 * @author Michael Ostner
 *
 */
public class ApplicationTest extends FunctionalTest {

	/**
	 * the automtic generated test case by the play framework
	 */
    @Test
    public void testThatIndexPageWorks() {
      final  Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }
    
}