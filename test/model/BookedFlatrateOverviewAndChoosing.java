package model;

import java.util.Calendar;

import models.BookedFlatrate;
import models.Flatrate;
import models.User;

import org.junit.Test;

import play.test.UnitTest;
import util.ActionUtil;

/**
 * 
 * @author Sebastian
 * 
 */
public class BookedFlatrateOverviewAndChoosing extends UnitTest {

	@Test
	public void bookNewFlatrateIfNoneExist() {
		// A User books a new Flatrate for the next Month, and no one exists
		User tmpUser = createUserAndSave();
		Flatrate flatrate = (Flatrate) Flatrate.findAll().get(0);

		ActionUtil.changeBookedFlatrateForNextMonth(tmpUser, flatrate);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);

		BookedFlatrate currBf = ActionUtil.getBookedFlatrate(tmpUser, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));

		assertNotNull(currBf);
		assertEquals(flatrate, currBf.getFlatrate());

		tmpUser.delete();
	}

	@Test
	public void bookNewFlatrateIfOneExist() {
		// A User changes his Flatrate for the next month, but he already has
		// one
		User tmpUser = createUserAndSave();
		Flatrate flatrate = (Flatrate) Flatrate.findAll().get(0);

		ActionUtil.changeBookedFlatrateForNextMonth(tmpUser, flatrate);

		Flatrate newFlatrate = (Flatrate) Flatrate.findAll().get(1);
		ActionUtil.changeBookedFlatrateForNextMonth(tmpUser, newFlatrate);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);

		BookedFlatrate currBf = ActionUtil.getBookedFlatrate(tmpUser, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));

		assertNotNull(currBf);
		assertNotSame(flatrate, newFlatrate);
		assertEquals(newFlatrate, currBf.getFlatrate());

		tmpUser.delete();
	}

	@Test
	public void deleteBookedFlatrate() {
		// A User doesn´t want to have a Flatrate next month
		User tmpUser = createUserAndSave();
		Flatrate flatrate = (Flatrate) Flatrate.findAll().get(0);

		// create a dummy Flatrate
		ActionUtil.changeBookedFlatrateForNextMonth(tmpUser, flatrate);
		// Calendar cal = Calendar.getInstance();
		// cal.add(Calendar.MONTH, 1);
		BookedFlatrate currBf = ActionUtil.getBookedFlatrateForNextMonth(tmpUser);
		// //Test if there is a Flatrate booked
		assertNotNull(currBf);

		// Delete Flatrate
		ActionUtil.changeBookedFlatrateForNextMonth(tmpUser, null);

		// Test if deletion was OK?
		// currBf = ActionUtil.getBookedFlatrateForCurrentMonth(tmpUser);
		// assertNull(currBf);

		tmpUser.delete();
	}

	@Test
	public void upgradeCurrentFlatrate() {
		// A user has a flatrate and want to upgrade it in current Month, his
		// flatrate is upgradable (has lower priority than new one)
		User tmpUser = createUserAndSave();
		Flatrate flatrate = (Flatrate) Flatrate.findAll().get(0);

		// initially creates bookedFlatrate
		ActionUtil.upgradeBookedFlatrate(flatrate, tmpUser);
		BookedFlatrate currBf = ActionUtil.getBookedFlatrateForCurrentMonth(tmpUser);
		assertNotNull(currBf);
		assertEquals(flatrate, currBf.getFlatrate());

		// upgrade it to a higher one
		Flatrate newFlatrate = (Flatrate) Flatrate.findAll().get(1);
		ActionUtil.upgradeBookedFlatrate(newFlatrate, tmpUser);

		currBf = ActionUtil.getBookedFlatrateForCurrentMonth(tmpUser);

		assertNotNull(currBf);
		assertNotSame(flatrate, newFlatrate);
		assertEquals(newFlatrate, currBf.getFlatrate());

		tmpUser.delete();
	}

	@Test
	public void downgradeCurrentFlatrate() {
		// A user has a flatrate and want to upgrade it in current Month,but his
		// flatrate is not upgradable (has higher priority than new one)
		// -> he want to downgrade because he has not used it completely
		User tmpUser = createUserAndSave();
		Flatrate flatrate = (Flatrate) Flatrate.findAll().get(2);

		// initially creates bookedFlatrate
		ActionUtil.upgradeBookedFlatrate(flatrate, tmpUser);
		BookedFlatrate currBf = ActionUtil.getBookedFlatrateForCurrentMonth(tmpUser);
		assertNotNull(currBf);
		assertEquals(flatrate, currBf.getFlatrate());

		// upgrade it to a higher one
		Flatrate newFlatrate = (Flatrate) Flatrate.findAll().get(1);
		ActionUtil.upgradeBookedFlatrate(newFlatrate, tmpUser);

		currBf = ActionUtil.getBookedFlatrateForCurrentMonth(tmpUser);

		// check if it is not null and if the flatrates are not the same
		assertNotNull(currBf);
		assertNotSame(flatrate, newFlatrate);
		// check if change has not happen
		assertNotSame(newFlatrate, currBf.getFlatrate());
		// check if flatrate is old flatrate
		assertEquals(flatrate, currBf.getFlatrate());

		tmpUser.delete();
	}

	private User createUserAndSave() {
		// Create a new user and save it
		User u = new User("name1", "name2", "name@name.de", "", "", "", "", "", "");
		u.save();
		return u;
	}

}
