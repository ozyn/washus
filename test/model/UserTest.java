package model;

import models.User;

import org.junit.Test;

import play.test.UnitTest;

/**
 * tests for evaluating the {@link User} model
 * 
 * @author Michael Ostner
 * 
 */
public class UserTest extends UnitTest {

	@Test
	public void createAndRetrieveUser() {

		final User tmpUser = createUserAndSave();

		// Retrieve the user with e-mail address bob@gmail.com
		final User user = User.find("byFirstnameAndLastname", "Michael", "Ostner").first();

		// Test
		assertNotNull(user);
		assertEquals("ostner@in.tum.de", user.getEmail());

		tmpUser.delete();
	}

	@Test
	public void tryConnectAsUser() {
		User u = createUserAndSave();

		// Test
		assertNotNull(User.connect("ostner@in.tum.de", "mySecret"));
		assertNull(User.connect("ostner@in.tum.de", "badpassword"));
		assertNull(User.connect("Jemand", "mySecret"));

		u.delete();
	}

	@Test
	public void passwordIsNotSavedInPlain() {
		User u = createUserAndSave();

		// Retrieve the user with e-mail address bob@gmail.com
		User user = User.find("byFirstnameAndLastname", "Michael", "Ostner").first();

		assertFalse(user.getPassword().equals("mySecret"));

		u.delete();
	}

	private User createUserAndSave() {
		// Create a new user and save it
		User u = new User("Michael", "Ostner", "ostner@in.tum.de", "mySecret", "a Street", "12b", "86523", "doofhausen", "m").save();
		return u;
	}
}
