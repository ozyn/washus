package models;

import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;

/**
 * 
 * @author Sebastian
 * 
 */
@Entity
public class FlatrateItem extends Model  {

	@NotNull
	@ManyToOne(fetch=FetchType.EAGER)
	public Flatrate flatrate;
	
	@NotNull
	@ManyToOne(fetch=FetchType.EAGER)
	public LaundryItem laundryItem;

	public int capacity;

	public FlatrateItem(Flatrate flatrate, LaundryItem laundryItem, int capacity) {
		super();
		this.flatrate = flatrate;
		this.laundryItem = laundryItem;
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		return "FlatrateItem [flatrate=" + flatrate + ", laundryItem=" + laundryItem + ", capacity=" + capacity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + capacity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlatrateItem other = (FlatrateItem) obj;
		if (capacity != other.capacity)
			return false;
		return true;
	}



	
	
	
	
	


	
	

		
	

}
