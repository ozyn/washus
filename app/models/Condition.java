package models;

import java.util.Date;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;
import util.hash.PasswordHasher;
import util.hash.Sha256Hasher;

/**
 * Mappingklasse zwischen Laundry und LaundryItem. 
 * Gibt an welche Konditionen eine Laundry für die dort angebotenen Leistungen hat.
 * @author Sebastian
 *
 */
@Entity
public class Condition extends Model {
	
	@NotNull
	@ManyToOne
	private Laundry laundry;
	
	@NotNull
	@ManyToOne
	private LaundryItem laundryItem;
	
	private double price;



	public Condition(Laundry laundry, LaundryItem laundryItem,
			double pricePerItem) {
		super();
		this.laundry = laundry;
		this.laundryItem = laundryItem;
		this.price = pricePerItem;
	}

	@Override
	public String toString() {
		return "Condition [laundry=" + laundry + ", laundryItem=" + laundryItem + ", price=" + price + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condition other = (Condition) obj;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}

	
	public Laundry getLaundry() {
		return laundry;
	}

	public void setLaundry(Laundry laundry) {
		this.laundry = laundry;
		
		if(!laundry.getConditions().contains(this)){
			laundry.addCondition(this);
		}
	}

	public LaundryItem getLaundryItem() {
		return laundryItem;
	}

	public void setLaundryItem(LaundryItem laundryItem) {
		this.laundryItem = laundryItem;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	


	
}
