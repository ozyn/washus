package models;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import play.db.jpa.Model;

/**
 * Mappingklasse zwischen Laundry und LaundryItem. Gibt an welche Konditionen
 * eine Laundry für die dort angebotenen Leistungen hat.
 * 
 * @author Sebastian
 * 
 */
@Entity
public class Usage extends Model {

	@NotNull
	@ManyToOne
	private Condition condition;

	@ManyToOne
	@NotNull
	private BookedFlatrate bookedFlatrate;

	@ManyToOne
	private Hotel hotel;

	private Date datePurchased;

	private int ratingLaundry;
	private int ratingHotel;

	public Usage(Condition condition, BookedFlatrate bookedFlatrate, Hotel hotel, Date datePurchased, int ratingLaundry, int ratingHotel) {
		super();
		this.condition = condition;
		setBookedFlatrate(bookedFlatrate);
		this.hotel = hotel;
		this.datePurchased = datePurchased;
		this.ratingLaundry = ratingLaundry;
		this.ratingHotel = ratingHotel;

	}

	public Usage(Condition condition, BookedFlatrate bookedFlatrate, Hotel hotel, Date datePurchased) {
		super();
		this.condition = condition;
		setBookedFlatrate(bookedFlatrate);
		this.hotel = hotel;
		this.datePurchased = datePurchased;

	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public BookedFlatrate getBookedFlatrate() {
		return bookedFlatrate;
	}

	public void setBookedFlatrate(BookedFlatrate bookedFlatrate) {
		this.bookedFlatrate = bookedFlatrate;
		
		if(!bookedFlatrate.getUsages().contains(this)){
			bookedFlatrate.getUsages().add(this);
		}
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Date getDatePurchased() {
		return datePurchased;
	}

	public void setDatePurchased(Date datePurchased) {
		this.datePurchased = datePurchased;
	}

	public int getRatingLaundry() {
		return ratingLaundry;
	}

	public void setRatingLaundry(int ratingLaundry) {
		this.ratingLaundry = ratingLaundry;
	}

	public int getRatingHotel() {
		return ratingHotel;
	}

	public void setRatingHotel(int ratingHotel) {
		this.ratingHotel = ratingHotel;
	}

	@Override
	public String toString() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(datePurchased);
		return "\tUsage [" + condition.getLaundryItem().name + ", for ("+cal.get(Calendar.DAY_OF_MONTH)+"/" + bookedFlatrate.getBookedMonth() + "/" + bookedFlatrate.getBookedYear() + ") in " + condition.getLaundry().getName() + ", hotel=" + hotel
				+ "(" + ")]";
	}

}
