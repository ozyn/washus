package models;

import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;

/**
 * 
 * @author Sebastian
 * 
 */
@Entity
public class Flatrate extends Model {

	private double price;
	private String name;
	private int hierarchy;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<FlatrateItem> flatrateItems;

	public Flatrate(double price, String name, Set<FlatrateItem> flatrateItems) {
		super();
		this.price = price;
		this.name = name;
		this.flatrateItems = flatrateItems;
	}

	@Override
	public String toString() {
		return "Flatrate [price=" + price + ", name=" + name + "]";
	}

	public boolean isUpgradable(Flatrate f) {
		if (this.hierarchy < f.getHierarchy()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Flatrate other = (Flatrate) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}

	public int getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(int hierarchy) {
		this.hierarchy = hierarchy;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<FlatrateItem> getFlatrateItems() {
		return flatrateItems;
	}

	public void setFlatrateItems(Set<FlatrateItem> flatrateItems) {
		this.flatrateItems = flatrateItems;
	}

}
