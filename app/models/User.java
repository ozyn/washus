package models;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import play.Logger;
import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.modules.guice.InjectSupport;
import util.hash.PasswordHasher;

/**
 * A straight forward entity class
 * 
 * @author Michael Ostner
 * 
 */
@Entity
@InjectSupport
public class User extends Model {

	/**
	 * the users firstname
	 */
	@Required
	private String firstname;

	/**
	 * the users lastname
	 */
	@Required
	private String lastname;

	/**
	 * the users email
	 */
	@Email
	@Required
	private String email;

	/**
	 * the users password hash
	 */
	@Required
	private String password;

	/**
	 * the users street
	 */
	@Required
	private String street;

	/**
	 * the users house number
	 */
	@Required
	private String number;

	/**
	 * the users zip code
	 */
	@Required
	private String zipCode;

	/**
	 * the users city
	 */
	@Required
	private String city;

	/**
	 * the users gender
	 */
	@Required
	private String gender;

	@ManyToOne(fetch = FetchType.EAGER)
	private Role role;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<BookedFlatrate> bookedFlatrates;

	@ManyToMany
	private Set<Hotel> hotels;

	@OneToOne(fetch = FetchType.EAGER)
	private Laundry laundry;

	@Inject
	transient private static PasswordHasher hasher;

	private String key;

	@Required
	private String pin;

	public User(final String firstname, final String lastname, final String email, final String password, final String street, final String number, final String zipCode, final String city, final String gender) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		setPassword(password);
		this.street = street;
		this.number = number;
		this.zipCode = zipCode;
		this.city = city;
		this.gender = gender;
		this.key = getNewKey();
	}

	public static String getNewKey() {
		final Random r = new Random();
		// maximum for our key is ZZZZZ -> Integer.parseInt("ZZZZZ", 36) =
		// 60466175
		final int max = 60466175;
		final int min = 11111;

		// find a number between min and max
		int random = r.nextInt((max - min) + 1) + min;

		// get the string representation
		String key = Integer.toString(random, 36).toUpperCase();

		// find a key that no user currently has
		while (find("byKey", key).first() != null) {
			random = r.nextInt((max - min) + 1) + min;
			key = Integer.toString(random, 36).toUpperCase();
		}

		return key;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname
	 *            the firstname to set
	 */
	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname
	 *            the lastname to set
	 */
	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(final String password) {
		this.password = hasher.hash(password);
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(final String street) {
		this.street = street;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(final String number) {
		this.number = number;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(final String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(final String city) {
		this.city = city;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(final String gender) {
		this.gender = gender;
	}

	/**
	 * @return the roles
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRole(final Role role) {
		this.role = role;
	}

	/**
	 * @return the hotels
	 */
	public Set<Hotel> getHotels() {
		return hotels;
	}

	/**
	 * @param hotels
	 *            the hotels to set
	 */
	public void setHotels(final Set<Hotel> hotels) {
		this.hotels = hotels;
	}

	/**
	 * @return the laundry
	 */
	public Laundry getLaundry() {
		return laundry;
	}

	/**
	 * @param laundry
	 *            the laundry to set
	 */
	public void setLaundry(final Laundry laundry) {
		this.laundry = laundry;
	}

	/**
	 * searches an user by email and password. type in the password in plain
	 * because it will automatically be hashed.
	 * 
	 * @param email
	 * @param password
	 * @return
	 */
	public static User connect(final String email, final String password) {
		return find("byEmailAndPassword", email, hasher.hash(password)).first();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// return String.format("%s %s<%s>", getFirstname(), getLastname(),
		// getEmail());
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", password=" + password + ", street=" + street + ", number=" + number + ", zipCode=" + zipCode + ", city=" + city + ", gender=" + gender + ", role=" + role + ", bookedFlatrates="
				+ bookedFlatrates + ", hotels=" + hotels + ", laundry=" + laundry + ", key=" + key + ", pin=" + pin + "]";
	}

	/**
	 * find a user by his or her email address
	 * 
	 * @param email
	 * @return
	 */
	public static User findByEmail(final String email) {
		if (email == null) {
			Logger.warn("could not get user by email. Email was null!");
			return null;
		}
		final User user = find("byEmail", email).first();

		if (user == null)
			Logger.warn("searched for user '%s' but none was found", email);
		return user;
	}

	/**
	 * returns true if a user has a certain role
	 * 
	 * @param role
	 * @return
	 */
	public boolean hasRole(final String role) {
		Role r = Role.findByName(role);
		if (r == null && Logger.isEnabledFor("error")) {
			// log IS surrounded by an if clause
			Logger.error("cannot find role '%s' in database", role); // NOPMD
		}

		return getRole().equals(r);
	}

	public Set<BookedFlatrate> getBookedFlatrates() {
		if (bookedFlatrates == null) {
			bookedFlatrates = new LinkedHashSet<>();
		}
		return bookedFlatrates;
	}

	public void setBookedFlatrates(Set<BookedFlatrate> bookedFlatrates) {
		this.bookedFlatrates = bookedFlatrates;
	}

	public BookedFlatrate addBookedFlatrate(BookedFlatrate bf) {
		this.bookedFlatrates.add(bf);
		if (bf.getUser() == null) {
			bf.setUser(this);
		}

		return bf;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	/**
	 * checks if other has differences in certain fields and updates them. this
	 * method does not persist the entity. The fields:
	 * 'firstname','lastname','email','gender','street','number','zipCode','city'
	 * , ' p i n ' . All others must be handled speratly!
	 * 
	 * @param other
	 */
	public void updatePreferences(User other) {
		Logger.debug("updating user id: %d", this.getId());
		updateFieldIfDiffers("firstname", other);
		updateFieldIfDiffers("lastname", other);
		updateFieldIfDiffers("email", other);
		updateFieldIfDiffers("street", other);
		updateFieldIfDiffers("number", other);
		updateFieldIfDiffers("zipCode", other);
		updateFieldIfDiffers("city", other);
		updateFieldIfDiffers("pin", other);
		updateFieldIfDiffers("gender", other);
	}

	private void updateFieldIfDiffers(String fieldName, User other) {
		try {
			Field field = User.class.getDeclaredField(fieldName);
			final Object thisValue = field.get(this);
			final Object otherValue = field.get(other);

			if (thisValue == null || !thisValue.equals(otherValue)) {
				Logger.debug("changing this: '%s' to other: '%s'", thisValue, otherValue);
				field.set(this, otherValue);
			}

		} catch (RuntimeException e) {
			Logger.error(e, "%s", e.getMessage());
			throw e;
		} catch (Exception e) {
			Logger.error(e, "%s", e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

}
