package models;

import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;

/**
 * 
 * @author Sebastian
 * 
 */
@Entity
public class LaundryItem extends Model {

	public String name;
	public String itemDescription;

	public LaundryItem(String name, String itemDescription) {
		super();
		this.name = name;
		this.itemDescription = itemDescription;
	}

	@Override
	public String toString() {
		return "\n\t\t->LaundryItem [name=" + name + ", itemDescription=" + itemDescription + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((itemDescription == null) ? 0 : itemDescription.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LaundryItem other = (LaundryItem) obj;
		if (itemDescription == null) {
			if (other.itemDescription != null)
				return false;
		} else if (!itemDescription.equals(other.itemDescription))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



}
