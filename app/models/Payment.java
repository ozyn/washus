package models;

import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;

/**
 * 
 * @author Sebastian
 * 
 */
@Entity
public class Payment extends Model {

	public String bankName;
	public String bic;
	public String iban;
	public String ccno;
	public String ccCustomerName;
	public String ccCompanyName;
	public String paypalAccount;
	
	
	public Payment(String bankName, String bic, String iban, String ccno, String ccCustomerName, String ccCompanyName, String paypalAccount) {
		super();
		this.bankName = bankName;
		this.bic = bic;
		this.iban = iban;
		this.ccno = ccno;
		this.ccCustomerName = ccCustomerName;
		this.ccCompanyName = ccCompanyName;
		this.paypalAccount = paypalAccount;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((bic == null) ? 0 : bic.hashCode());
		result = prime * result + ((ccCompanyName == null) ? 0 : ccCompanyName.hashCode());
		result = prime * result + ((ccCustomerName == null) ? 0 : ccCustomerName.hashCode());
		result = prime * result + ((ccno == null) ? 0 : ccno.hashCode());
		result = prime * result + ((iban == null) ? 0 : iban.hashCode());
		result = prime * result + ((paypalAccount == null) ? 0 : paypalAccount.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (bankName == null) {
			if (other.bankName != null)
				return false;
		} else if (!bankName.equals(other.bankName))
			return false;
		if (bic == null) {
			if (other.bic != null)
				return false;
		} else if (!bic.equals(other.bic))
			return false;
		if (ccCompanyName == null) {
			if (other.ccCompanyName != null)
				return false;
		} else if (!ccCompanyName.equals(other.ccCompanyName))
			return false;
		if (ccCustomerName == null) {
			if (other.ccCustomerName != null)
				return false;
		} else if (!ccCustomerName.equals(other.ccCustomerName))
			return false;
		if (ccno == null) {
			if (other.ccno != null)
				return false;
		} else if (!ccno.equals(other.ccno))
			return false;
		if (iban == null) {
			if (other.iban != null)
				return false;
		} else if (!iban.equals(other.iban))
			return false;
		if (paypalAccount == null) {
			if (other.paypalAccount != null)
				return false;
		} else if (!paypalAccount.equals(other.paypalAccount))
			return false;
		return true;
	}


	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Payment [bankName=" + bankName + ", bic=" + bic + ", iban=" + iban + ", ccno=" + ccno + ", ccCustomerName=" + ccCustomerName + ", ccCompanyName=" + ccCompanyName + ", paypalAccount=" + paypalAccount + "]";
	}



	
	

	
	
}
