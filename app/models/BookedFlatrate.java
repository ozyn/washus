package models;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import play.db.jpa.Model;

/**
 * Mappingklasse zwischen User und Flatrate
 * 
 * @author Sebastian
 * 
 */
@Entity
public class BookedFlatrate extends Model {

	@NotNull
	@ManyToOne
	private User user;

	@NotNull
	@ManyToOne
	private Flatrate flatrate;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Usage> usages;

	private Date contractDate;

	@NotNull
	private int bookedMonth;
	@NotNull
	private int bookedYear;

	public BookedFlatrate(User user, Flatrate flatrate, Date contractDate, Integer bookedMonth, Integer bookedYear) {
		super();
		setUser(user);
		this.flatrate = flatrate;

		if (contractDate != null) {
			this.contractDate = contractDate;
		} else {
			this.contractDate = Calendar.getInstance().getTime();
		}

		if (bookedMonth == null || bookedYear == null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.contractDate);
			cal.add(Calendar.MONTH, 1);

			this.bookedMonth = cal.get(Calendar.MONTH);
			this.bookedYear = cal.get(Calendar.YEAR);
		} else {
			this.bookedMonth = bookedMonth;
			this.bookedYear = bookedYear;
		}

	}

	@Override
	public String toString() {
		return "\nBookedFlatrate{"+id+"}[" + flatrate.getName() + "(" + bookedMonth + "/" + bookedYear + "),user=" + user.getEmail() + ",  date=" + contractDate.getDay() + "." + contractDate.getMonth() + "."
				+ contractDate.getYear() + ", usages=" + usages + "]";
	}


	public void setUser(User user){
		this.user = user;
		
		if(!user.getBookedFlatrates().contains(this)){
			user.getBookedFlatrates().add(this);
		}
	}

	public Flatrate getFlatrate() {
		return flatrate;
	}

	public void setFlatrate(Flatrate flatrate) {
		this.flatrate = flatrate;
	}

	public Set<Usage> getUsages() {
		if(usages == null){
			this.usages = new LinkedHashSet<>();
		}
		return usages;
	}

	public void setUsages(Set<Usage> usages) {
		this.usages = usages;
	}
	
	public void addUsage(Usage usage){
		this.usages.add(usage);
		
		if(usage.getBookedFlatrate() != this){
			usage.setBookedFlatrate(this);
		}
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public int getBookedMonth() {
		return bookedMonth;
	}

	public void setBookedMonth(int bookedMonth) {
		this.bookedMonth = bookedMonth;
	}

	public int getBookedYear() {
		return bookedYear;
	}

	public void setBookedYear(int bookedYear) {
		this.bookedYear = bookedYear;
	}

	public User getUser() {
		return user;
	}	
	

}
