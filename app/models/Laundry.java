package models;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.google.gson.annotations.Expose;

import play.db.jpa.Model;
import play.modules.guice.InjectSupport;

/**
 * 
 * @author Sebastian
 * 
 */
@Entity
public class Laundry extends Model{

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@OneToOne(fetch = FetchType.EAGER)
	private Payment bankAccount;
	
	@OneToMany(cascade=CascadeType.ALL)
	private Set<Condition> conditions;

	@Expose // include this variable when parsing this to json
	private String name;
	
	private String email;
	
	@Expose // include this variable when parsing this to json
	private String street;
	
	@Expose // include this variable when parsing this to json
	private String number;
	
	@Expose // include this variable when parsing this to json
	private String postCode;
	
	@Expose // include this variable when parsing this to json
	private String city;
	
	@Expose // include this variable when parsing this to json
	private double lat, lng;
	
	
	
	public Laundry(User user, Payment bankAccount, Set<Condition> conditions, String name, String email, String street, String number, String postCode, String city, double lat, double lng) {
		super();
		this.user = user;
		this.bankAccount = bankAccount;
		this.conditions = conditions;
		this.name = name;
		this.email = email;
		this.street = street;
		this.number = number;
		this.postCode = postCode;
		this.city = city;
		this.lat = lat;
		this.lng = lng;
	}

	

	@Override
	public String toString() {
		return "Laundry [name=" + name + ", email=" + email + ", street=" + street + ", number=" + number + ", postCode=" + postCode + ", city=" + city + ", lat=" + lat + ", lng=" + lng + "]";
	}
	
	public void addCondition(Condition c){
		if(conditions == null){
			conditions = new LinkedHashSet<>();
		}
		conditions.add(c);
		if(c.getLaundry() != this){
			c.setLaundry(this);
		}
	}

	

	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Payment getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(Payment bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Set<Condition> getConditions() {
		return conditions;
	}

	public void setConditions(Set<Condition> conditions) {
		this.conditions = conditions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
	
	
	

}
