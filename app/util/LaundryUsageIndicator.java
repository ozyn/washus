package util;

import java.util.Date;

import models.BookedFlatrate;
import models.LaundryItem;

/**
 * for laundry-dashboard (indicates usages of all customers for laundry incl. the money earned)
 * @author Sebastian
 * 
 */
public class LaundryUsageIndicator {

	private final LaundryItem laundryItem;
	private int itemsUsed;
	private double moneyEarned;
	private double price;
	private Date date;

	public LaundryUsageIndicator(LaundryItem laundryItem, long itemsUsed, double price) {
		super();
		this.laundryItem = laundryItem;
		this.itemsUsed = (new Long(itemsUsed)).intValue();
		this.price = price;
		moneyEarned = price * itemsUsed;
		this.date = null;
	}
	

	public LaundryUsageIndicator(LaundryItem laundryItem, long itemsUsed, double price, Date date) {
		super();
		this.laundryItem = laundryItem;
		this.itemsUsed = (new Long(itemsUsed)).intValue();
		this.price = price;
		this.date = date;
		moneyEarned = price * itemsUsed;
	}

	public Date getDate() {
		return date;
	}

	public LaundryItem getLaundryItem() {
		return laundryItem;
	}

	public int getItemsUsed() {
		return itemsUsed;
	}

	public double getMoneyEarned() {
		return moneyEarned;
	}

	public double getPrice() {
		return price;
	}
	

	public void setItemsUsed(int itemsUsed) {
		this.itemsUsed = itemsUsed;
	}


	public void setMoneyEarned(double moneyEarned) {
		this.moneyEarned = moneyEarned;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "LaundryUsageIndicator [laundryItem=" + laundryItem.name + ", itemsUsed=" + itemsUsed + ", moneyEarned=" + moneyEarned + ", price=" + price + "/"+date+"]";
	}
	
	public void addItems(Integer itemCount){
		this.itemsUsed += itemCount;
		moneyEarned = price * itemsUsed;
	}
	
	
	
}
