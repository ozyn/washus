package util.ip.publicIpResolverImpl;

import util.ip.PublicIpResolver;

/**
 * @see PublicIpResolver
 * @author Michael Ostner
 *
 */
public class IPecho extends AbstractPublicIpResolver{

	/**
	 * returns <code>http://ipecho.net/plain</code>
	 */
	@Override
	protected String getUrlString() {
		return "http://ipecho.net/plain";
	}

	/**
	 * @param fullResponse
	 * @return the value of fullResponse trimmed
	 */
	@Override
	protected String extractIp(final String fullResponse) {
		String value = null;// NOPMD - Annotation for the PMD plugin to not
		// mark this line
		if (fullResponse != null) {
			value = fullResponse.trim();
		}

		return value;
	}
}
