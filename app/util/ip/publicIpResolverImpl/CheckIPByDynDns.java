package util.ip.publicIpResolverImpl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.ip.PublicIpResolver;

/**
 * @see PublicIpResolver
 * @author Michael Ostner
 * 
 */
public class CheckIPByDynDns extends AbstractPublicIpResolver {
	/**
	 * The url as string of this service
	 */
	public static final String URL = "http://checkip.dyndns.org/";

	/**
	 * The classical pattern of an IPv4
	 */
	private static final Pattern IP_PATTERN = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");

	/**
	 * returns {@value #URL}
	 */
	@Override
	protected String getUrlString() {
		return URL;
	}

	/**
	 * @param fullResponse
	 * @return the value of fullResponse trimmed
	 */
	@Override
	protected String extractIp(final String fullResponse) {
		String value = null;// NOPMD - Annotation for the PMD plugin to not mark
							// this line
		if (fullResponse != null) {
			final Matcher matcher = IP_PATTERN.matcher(fullResponse);
			matcher.find(); //NOPMD
			value = matcher.group(); // NOPMD
		}

		return value;
	}
}
