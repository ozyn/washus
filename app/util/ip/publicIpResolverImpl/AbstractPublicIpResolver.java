package util.ip.publicIpResolverImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import util.ip.PublicIpResolver;

/**
 * this class gives to simple abstract methods for getting the content of an URL
 * and parsing it for getting the public IP of the current system
 * 
 * @author Michael Ostner
 * 
 */
public abstract class AbstractPublicIpResolver implements PublicIpResolver {

	private URL getURL() {
		try {
			return new URL(getUrlString());
		} catch (MalformedURLException e) {
			throw new RuntimeException(e.getMessage(), e); // NOPMD - this
															// exception will
															// not happen
		}
	}

	/**
	 * get the specific URL from the subclasses
	 */
	protected abstract String getUrlString();

	/**
	 * extracts the IP from the full content got by {@link #getURL()}
	 * 
	 * @param next
	 * @return
	 */
	protected abstract String extractIp(String next);

	/**
	 * find the public IP using the specific service of this class
	 */
	@Override
	public String findPublicIp() throws IOException {
		try (Scanner initScanner = new Scanner(this.getURL().openStream()); Scanner scanner = initScanner.useDelimiter("\\A")) {
			return extractIp(scanner.next());
		}
	}
}
