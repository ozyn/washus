package util.ip.publicIpResolverImpl;

import util.ip.PublicIpResolver;

/**
 * @see PublicIpResolver
 * @author Michael Ostner
 *
 */
public class IdentMeIPv4 extends AbstractPublicIpResolver {
	/**
	 * The url as string of this service
	 */
	public static final String URL = "http://v4.ident.me";

	/**
	 * returns {@value #URL}
	 */
	@Override
	protected String getUrlString() {
		return URL;
	}

	/**
	 * @param fullResponse
	 * @return the value of fullResponse trimmed
	 */
	@Override
	protected String extractIp(final String fullResponse) {
		String value = null;// NOPMD - Annotation for the PMD plugin to not
		// mark this line
		if (fullResponse != null) {
			value = fullResponse.trim();
		}

		return value;
	}
}
