package util.ip;


public class PrivatePublicIpDistinguisherImpl implements PrivatePublicIpDistinguisher {

	@Override
	public boolean isLocaleIp(String ip) {
		switch (ip) {
		case "0:0:0:0:0:0:0:1":
		case "127.0.0.1":
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean isPublicIp(String ip) {
		return !isLocaleIp(ip);
	}

}
