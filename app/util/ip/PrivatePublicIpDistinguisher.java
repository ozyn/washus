package util.ip;

import javax.mail.internet.InternetAddress;

/**
 * Implementations of this class are able to distinguish if a given IP is from
 * the locale subnet or if it is a the public ip given by the ISP
 * 
 * @author Michael Ostner
 * 
 */
public interface PrivatePublicIpDistinguisher {
	public boolean isLocaleIp(String ip);

	public boolean isPublicIp(String ip);
}
