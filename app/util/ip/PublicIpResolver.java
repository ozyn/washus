package util.ip;

import java.io.IOException;

/**
 * Implementations of this class can determine the public IP of the server.
 * Probably by calling to an external service.
 * 
 * @author Michael Ostner
 * 
 */
public interface PublicIpResolver {
	public String findPublicIp() throws IOException;
}
