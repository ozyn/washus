package util;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Query;

import com.google.common.base.Preconditions;

import controllers.Security;
import play.Logger;
import play.db.jpa.JPA;
import play.mvc.Scope.Session;
import models.BookedFlatrate;
import models.Condition;
import models.Flatrate;
import models.FlatrateItem;
import models.Hotel;
import models.Laundry;
import models.LaundryItem;
import models.Usage;
import models.User;

/**
 * 
 * @author Sebastian
 * 
 */
public class ActionUtil {

	/**
	 * @return bookedFlatrate which is currently in use (for current month and
	 *         year), null if none is in use
	 */
	public static BookedFlatrate getBookedFlatrateForCurrentMonth(User user) {
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		return getBookedFlatrate(user, year, month);
	}

	/**
	 * @return bookedFlatrate for next month , null if there is none
	 */
	public static BookedFlatrate getBookedFlatrateForNextMonth(User user) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		BookedFlatrate bfNext = getBookedFlatrate(user, year, month);

		return bfNext;
	}

	/**
	 * 
	 * @param year
	 *            year of bookedFlatrate
	 * @param month
	 *            month of bookedFlatrate
	 * @return bookedFlatrate of given year and month, null if none exist
	 */
	public static BookedFlatrate getBookedFlatrate(User user, int year, int month) {
		if (user == null) {
			return null;
		}

		Query query = JPA.em().createQuery("SELECT distinct bf FROM BookedFlatrate bf left join fetch bf.usages WHERE bf.bookedMonth = :month AND bf.bookedYear = :year AND bf.user.id=:userId");
		query.setParameter("month", month);
		query.setParameter("year", year);
		query.setParameter("userId", user.id);

		// query.setMaxResults(1);

		List<BookedFlatrate> bfL = query.getResultList();

		if (bfL != null && bfL.size() > 0) {
			return bfL.get(0);
		} else {
			return null;
		}

	}

	/**
	 * Change given bookedFlatrate for given parameters
	 * 
	 * @param user
	 *            must be != null ->false
	 * @param bf
	 *            must be != null or ->false
	 * @param flatrate
	 *            if != null flatrate will be changed, if==null bookedFlatrate
	 *            will be deleted!
	 * @return true if change was successful, else false
	 */
	public static boolean changeBookedFlatrate(User user, BookedFlatrate bf, Flatrate flatrate) {
//		Logger.debug("user=%s, bf=%s,flatrate=%s", user, bf, flatrate);
		if (user == null || bf == null) {
			return false;
		}
		
		//remove?
		
		boolean contains = false;
		for(BookedFlatrate bf2 : user.getBookedFlatrates()){
			if(bf2.equals(bf)){
				contains = true;
			}
		}
		if (flatrate == null && contains) {
			user.getBookedFlatrates().remove(bf);
			user.save();
			return true;
		}

		bf.setFlatrate(flatrate);
		bf.save();

		return true;
	}

	/**
	 * change the bookedFlatrate for the given year and month. If none exist a
	 * new one will be created
	 */
	public static boolean changeBookedFlatrate(User user, int year, int month, Flatrate flatrate) {
		BookedFlatrate bf = getBookedFlatrate(user, year, month);
		if (bf == null && flatrate != null) {
			bf = new BookedFlatrate(user, flatrate, null, month, year);
			bf.save();
		}
		return changeBookedFlatrate(user, bf, flatrate);
	}

	/**
	 * changes the Flatrate for the next month
	 * 
	 * @param flatrate
	 *            bookedFlatrate will be changed to this Flatrate
	 * @return true if change was successfull;
	 */
	public static boolean changeBookedFlatrateForNextMonth(User user, Flatrate flatrate) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);

		return changeBookedFlatrate(user, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), flatrate);
	}

	/**
	 * changes the Flatrate for current Month, tests if change can happen (curr
	 * priority < new flatrate priority)
	 * 
	 * @param flatrate
	 *            bookedFlatrate will be changed to this Flatrat / if null ->
	 *            return false
	 * @param user
	 *            if null -> return false
	 * @return true if change was successfull;
	 */
	public static boolean upgradeBookedFlatrate(Flatrate flatrate, User user) {
		Calendar cal = Calendar.getInstance();
		BookedFlatrate bf = ActionUtil.getBookedFlatrateForCurrentMonth(user);

		if (flatrate == null || user == null) {
			return false;
		}

		if (bf == null || bf.getFlatrate().isUpgradable(flatrate)) {
			return changeBookedFlatrate(user, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), flatrate);
		}

		return false;
	}

	/**
	 * Adds a Usage of an Customer
	 * 
	 * @param condition
	 * @param bf
	 * @param hotel
	 * @return
	 */
	public static Usage addUsageForCurrentMonth(Condition condition, BookedFlatrate bf, Hotel hotel) {
		return addUsageForSpecificMonth(condition, bf, hotel, Calendar.getInstance().getTime());
	}

	/**
	 * Adds a Usage of an Customer for specifc Date
	 * 
	 */
	public static Usage addUsageForSpecificMonth(Condition condition, BookedFlatrate bf, Hotel hotel, Date usageDate) {
		Usage usage = new Usage(condition, bf, hotel, usageDate);
		// usage.create();
		bf.save(); // Cascade.all

		return usage;
	}

	public static BookedFlatrate addUsages(Map<Condition, Integer> newUsages, BookedFlatrate bf, Hotel hotel, Date usageDate) {
		for (Condition cond : newUsages.keySet()) {
			int count = newUsages.get(cond);
			for (int i = 0; i < count; i++) {
				bf.getUsages().add(new Usage(cond, bf, hotel, usageDate));
			}
		}
		bf.save();
		return bf;
	}

	public void createPayment(User user) {

	}

	/**
	 * returns the connected user for the session
	 * 
	 * @param session
	 * @return
	 */
	public static User getConnectedUser(final Session session) {
		final String connectedString = session.get("username");

		if (connectedString == null) {
			return null;
		} else {
			if (Logger.isTraceEnabled())
				Logger.trace("connected string = %s", connectedString);
		}

		final Long connected = Long.valueOf(connectedString);

		if (Logger.isTraceEnabled())
			Logger.trace("user '%s' is connected.", connected);

		final User foundUser = User.findById(connected);

		if (Logger.isTraceEnabled()) {
			Logger.trace("user '%s' was found: %5B", connected, foundUser != null);
		}

		return foundUser;
	}

	/**
	 * Get the sorted map of usageIndicators for given user in one map, only
	 * mapped by month/year
	 * 
	 * @see getDetailedUsageIndicatorsSorted(User u)
	 * @param u
	 * @return
	 */
	public static Map<String, List<DetailedUsageIndicator>> getDetailedUsageIndicatorsSortedOld(User u) {
		List<DetailedUsageIndicator> list = getDetailedUsageIndicators(u);
		Map<String, List<DetailedUsageIndicator>> result = new LinkedHashMap<>();

		for (DetailedUsageIndicator dui : list) {
			String key = dui.month + "/" + dui.year;

			if (result.get(key) == null) {
				List<DetailedUsageIndicator> temp = new LinkedList<>();
				temp.add(dui);
				result.put(key, temp);
			} else {
				result.get(key).add(dui);
			}

		}

		return result;
	}

	/**
	 * support a sorted map (of maps...) of the usages of given user (all time)
	 * maped by (month/year)->day in month->Laundry->LaundryItem with count
	 * 
	 * @param u
	 * @return
	 */
	public static Map<Long, Map<String, Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>>>> getDetailedUsageIndicatorsSorted(User u) {
		List<DetailedUsageIndicator> list = getDetailedUsageIndicators(u);

		Map<Long, Map<String, Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>>>> result = new LinkedHashMap<>();

		for (DetailedUsageIndicator dui : list) {

			// Month/year -> days
			// String key1 = dui.month +"" + dui.year;

			Calendar cal = Calendar.getInstance();
			cal.clear();
			cal.set(dui.year, dui.month, 1);
			Long key = cal.getTimeInMillis();

			// Monthyear
			String date = dui.month + "" + dui.year;
			Map<String, Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>>> dayMap = result.get(key);
			if (dayMap == null) {
				dayMap = new LinkedHashMap<String, Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>>>();
				result.put(key, dayMap);
			}

			// Day -> Laundry
			String dayKey = "" + dui.day;
			Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>> laundryMap = dayMap.get(dayKey);
			if (laundryMap == null) {
				laundryMap = new LinkedHashMap<Laundry, Map<LaundryItem, DetailedUsageIndicator>>();
				dayMap.put(dayKey, laundryMap);
			}

			// Laundry -> LaundryItem
			Laundry laundryKey = dui.laundry;
			Map<LaundryItem, DetailedUsageIndicator> laundryItemMap = laundryMap.get(laundryKey);
			if (laundryItemMap == null) {
				laundryItemMap = new LinkedHashMap<LaundryItem, DetailedUsageIndicator>();
				laundryMap.put(laundryKey, laundryItemMap);
			}

			// Laundry Item + dui
			LaundryItem laundryItemKey = dui.laundryItem;
			DetailedUsageIndicator currDui = laundryItemMap.get(laundryItemKey);
			 if(currDui != null){
				 currDui.count += dui.count;
			 } else {
				 laundryItemMap.put(laundryItemKey, dui);
			 }

		}

		return result;
	}

	/**
	 * get the capacity which is left of given bookedFlatrate
	 * 
	 * @param user
	 * @param bf
	 * @return
	 */
	public static Map<LaundryItem, Integer> getCapacityLeft(BookedFlatrate bf) {
		Map<LaundryItem, Integer> result = new LinkedHashMap<>();
		List<DetailedUsageIndicator> duiL = getDetailedUsageIndicators(bf);

		for (FlatrateItem fi : bf.getFlatrate().flatrateItems) {
			int capacity = fi.capacity;

			for (DetailedUsageIndicator dui : duiL) {
				if (dui.laundryItem == fi.laundryItem) {
					capacity -= dui.count;
				}
			}

			result.put(fi.laundryItem, capacity);
		}

		return result;
	}

	/**
	 * get detailedUsageIndicators of given bookedFlatrate
	 * 
	 * @param bf
	 * @return
	 */
	public static List<DetailedUsageIndicator> getDetailedUsageIndicators(BookedFlatrate bf) {
		Query query = JPA.em().createQuery(
				"SELECT distinct new util.DetailedUsageIndicator(bf.bookedMonth,bf.bookedYear,us.datePurchased,l,li,count(*)) " + "FROM Usage as us " + "join us.bookedFlatrate as bf "
						+ "join us.condition as c " + "join c.laundry as l " + "join c.laundryItem as li " + "WHERE bf = :bf " + "GROUP BY bf.bookedYear,bf.bookedMonth, us.datePurchased,l,li "
						+ "ORDER BY bf.bookedYear DESC,bf.bookedMonth DESC,us.datePurchased DESC,l DESC,li DESC");

		query.setParameter("bf", bf);
		List<DetailedUsageIndicator> result = query.getResultList();

		return result;
	}

	/**
	 * get detailedUsageIndicators of given user (all time)
	 * 
	 * @param u
	 * @return
	 */
	public static List<DetailedUsageIndicator> getDetailedUsageIndicators(User u) {
		Query query = JPA.em().createQuery(
				"SELECT distinct new util.DetailedUsageIndicator(bf.bookedMonth,bf.bookedYear,us.datePurchased,l,li,count(*)) " + "FROM Usage as us " + "join us.bookedFlatrate as bf "
						+ "join bf.user as user " + "join us.condition as c " + "join c.laundry as l " + "join c.laundryItem as li " + "where user = :u "
						+ "GROUP BY bf.bookedYear,bf.bookedMonth, us.datePurchased,l,li " + "ORDER BY bf.bookedYear DESC,bf.bookedMonth DESC,us.datePurchased ,l DESC,li DESC");

		query.setParameter("u", u);
		List<DetailedUsageIndicator> result = query.getResultList();
		return result;
	}

	/**
	 * get usages of given BookedFlatrate
	 * 
	 * @param bf
	 * @return list of all usageIndicators of given bf (incl. not used laundry
	 *         items), emtpy list if bf==null
	 */
	public static List<UsageIndicator> getUsageIndicators(BookedFlatrate bf) {
		if (bf == null) {
			return new LinkedList<UsageIndicator>();
		}
		Query query = JPA.em().createQuery(
				"SELECT distinct new util.UsageIndicator(li,count(*),item.capacity) " + "FROM Usage as us " + "join us.bookedFlatrate as bf " + "join bf.flatrate as flat "
						+ "join flat.flatrateItems as item " + "join item.laundryItem as li " + "join us.condition as cond " + "join cond.laundryItem as li2 " + "WHERE bf = :bf " + "AND li = li2 "
						+ "GROUP BY li " + "");

		query.setParameter("bf", bf);
		List<UsageIndicator> result = query.getResultList();

		// to get all "empty" laundry items
		for (FlatrateItem fi : bf.getFlatrate().flatrateItems) {
			boolean contain = false;
			for (UsageIndicator ui : result) {
				if (ui.getLaundryItem() == fi.laundryItem) {
					contain = true;
					break;
				}
			}
			if (!contain) {
				result.add(new UsageIndicator(fi.laundryItem, 0, fi.capacity));
			}
		}

		return result;
	}

	/**
	 * get all Usages of all customers for given laundries (1 user can have
	 * several laundries) for current month
	 * 
	 * @param laundries
	 * @return list of usages for given laundries (result list has no items with
	 *         0 count), if laundries==null -> empty list
	 */
	public static List<LaundryUsageIndicator> getLaundryUsageIndicator(Laundry laundry) {
		if (laundry == null) {
			return new LinkedList<LaundryUsageIndicator>();
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Query query = JPA.em().createQuery(
				"SELECT distinct new util.LaundryUsageIndicator(li,count(*),cond.price) " + "FROM Usage as us " + "join us.condition as cond " + "join cond.laundryItem as li "
						+ "join cond.laundry as l " + "WHERE l = :l " + "AND us.datePurchased >= :minTime " + "GROUP BY li " + "ORDER BY li ");

		query.setParameter("l", laundry);
		query.setParameter("minTime", cal.getTime());
		List<LaundryUsageIndicator> result = query.getResultList();
		return result;
	}

	public static Map<Long, List<LaundryUsageIndicator>> getLaundryUsageIndicator(Laundry laundry, int months) {
		if (laundry == null) {
			return new LinkedHashMap<Long, List<LaundryUsageIndicator>>();
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.MONTH, -months);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Query query = JPA.em().createQuery(
				"SELECT distinct new util.LaundryUsageIndicator(li,count(*),cond.price,us.datePurchased) " + "FROM Usage as us " + "join us.condition as cond " + "join cond.laundryItem as li "
						+ "join cond.laundry as l " + "WHERE l = :l " + "AND us.datePurchased >= :minTime " + "GROUP BY li, us.datePurchased " + "ORDER BY li ");

		query.setParameter("l", laundry);
		query.setParameter("minTime", cal.getTime());
		List<LaundryUsageIndicator> result = query.getResultList();
		
		

		Map<Long, List<LaundryUsageIndicator>> resultMap = new TreeMap<>(Collections.reverseOrder());

		for (LaundryUsageIndicator lui : result) {
			// Month/year -> days
			Calendar currCal = Calendar.getInstance();
			// currCal.clear();
			currCal.setTime(lui.getDate());
			currCal.set(Calendar.DAY_OF_MONTH, 1);
			currCal.set(Calendar.HOUR_OF_DAY, 0);
			currCal.set(Calendar.MINUTE, 0);
			currCal.set(Calendar.SECOND, 0);
			currCal.set(Calendar.MILLISECOND, 0);

			Long key = currCal.getTimeInMillis();

			List<LaundryUsageIndicator> currLuiList = resultMap.get(key);
			if (currLuiList == null) {
				// month/year not existing
				LinkedList<LaundryUsageIndicator> temp = new LinkedList<>();
				temp.add(lui);
				resultMap.put(key, temp);
			} else {
				//month/year existing
				//test if item already in month:
				LaundryUsageIndicator currLui = null;
				for (LaundryUsageIndicator tempLui : currLuiList) {
					if (tempLui.getLaundryItem() == lui.getLaundryItem()) {
						currLui = tempLui;
						break;
					}
				}
				if (currLui != null) {
					//found
					currLui.addItems(lui.getItemsUsed());
				} else {
					currLuiList.add(lui);
				}

			}
		}

		return resultMap;
	}

	public static List<LaundryBookItems> getLaundryBookItems(String key, BookedFlatrate bf) {

		Query query = JPA.em().createQuery(
				"SELECT distinct new util.LaundryBookItems(item,count(*)) " + 
		"FROM Usage as us " + "join us.bookedFlatrate as bf " + "join bf.user as user " + "join bf.flatrate as flat "
						+ "join flat.flatrateItems as item " + "join item.laundryItem as li " + "join us.condition as cond " + "join cond.laundryItem as li2 " + "WHERE user.key = :key "
						+ "AND li = li2 " + "AND bf = :bf " + "GROUP BY item " + "");

		query.setParameter("key", key);
		query.setParameter("bf", bf);

		List<LaundryBookItems> result = query.getResultList();

		// to get all "empty" laundry items
		for (FlatrateItem fi : bf.getFlatrate().flatrateItems) {
			boolean contain = false;
			for (LaundryBookItems ui : result) {
				if (ui.getLaundryItem() == fi.laundryItem) {
					contain = true;
					break;
				}
			}
			if (!contain) {
				result.add(new LaundryBookItems(fi, 0l));
			}
		}

		return result;
	}

}
