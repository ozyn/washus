package util;

import models.BookedFlatrate;
import models.FlatrateItem;
import models.LaundryItem;

/**
 * Indicates currentUsages in the context of booking new Usages in Laundries
 * @author Sebastian
 * 
 */
public class LaundryBookItems {

	private final LaundryItem laundryItem;
	private final int itemsUsed;
	private final int capacity;
	private int itemsLeft;
	private int numberOfBookings;

	public LaundryBookItems(LaundryItem laundryItem, long itemsUsed, int capacity) {
		super();
		this.laundryItem = laundryItem;
		this.itemsUsed = (new Long(itemsUsed)).intValue();
		this.capacity = capacity;
		this.setItemsLeft(this.capacity - this.itemsUsed);
		this.setNumberOfBookings(0);
	}
	
	public LaundryBookItems(FlatrateItem flatrateItem, long itemsUsed) {
		super();
		this.laundryItem = flatrateItem.laundryItem;
		this.itemsUsed = (new Long(itemsUsed)).intValue();
		this.capacity = flatrateItem.capacity;
		this.setItemsLeft(this.capacity - this.itemsUsed);
		this.setNumberOfBookings(0);
	}

	/**
	 * @return the laundryItem
	 */
	public LaundryItem getLaundryItem() {
		return laundryItem;
	}

	/**
	 * @return the itemsUsed
	 */
	public int getItemsUsed() {
		return itemsUsed;
	}

	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	@Override
	public String toString() {
		return "\nLaundryBookItems [laundryItem=" + laundryItem.name + ", itemsUsed=" + itemsUsed + ", capacity=" + capacity + ", itemsLeft=" + itemsLeft + ", numberOfBookings=" + numberOfBookings
				+  "]";
	}
	

	public int getItemsLeft() {
		return itemsLeft;
	}

	public void setItemsLeft(int itemsLeft) {
		this.itemsLeft = itemsLeft;
	}

	public int getNumberOfBookings() {
		return numberOfBookings;
	}

	public void setNumberOfBookings(int numberOfBookings) {
		this.numberOfBookings = numberOfBookings;
	}
	
	
}
