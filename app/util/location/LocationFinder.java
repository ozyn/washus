package util.location;

import java.io.IOException;

/**
 * Implementations of this interface are able to find the probable location of
 * an given IP.
 * 
 * @author Michael Ostner
 * 
 */
public interface LocationFinder {

	/**
	 * @param ipAddress
	 * @return gets the {@link Location} object for the given ipAddress
	 * @throws IOException
	 */
	Location getLocation(String ipAddress) throws IOException;
}
