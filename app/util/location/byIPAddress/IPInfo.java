package util.location.byIPAddress;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import play.Logger;
import util.location.Location;
import util.location.LocationFinder;

import com.google.gson.Gson;
import com.google.inject.internal.Preconditions;

/**
 * @see LocationFinder
 * @author Michael Ostner
 * 
 */
public class IPInfo implements LocationFinder {

	/**
	 * the URL pattern for getting the location by the ipinfo service
	 */
	private static final String URL = "http://ipinfo.io/%s/json";

	/**
	 * message if a error is thrown in {@link #getURL(String)}
	 */
	private static final String EXCEPTION_MESSAGE = "could not get url out of ip (%s)";

	/**
	 * the {@link Gson} object for parsing the response of the ipinfo service
	 */
	private transient final Gson gson;

	/**
	 * the constructor for this class
	 */
	public IPInfo() {
		this.gson = new Gson();
	}

	private URL getURL(final String ipAddress) {
		URL url = null; // NOPMD
		try {
			url = new URL(String.format(URL, ipAddress));
		} catch (MalformedURLException e) {
			if (Logger.isEnabledFor("ERROR")) {
				Logger.error(e, EXCEPTION_MESSAGE, ipAddress); // NOPMD -
				// the log is surrounded by a checking if
			}
		}
		return url;
	}

	/**
	 * gets the location from an certain ip
	 */
	public Location getLocation(final String ipAddress) throws IOException {
		Preconditions.checkNotNull(ipAddress, "the ip must not be null");

		final URL url = getURL(ipAddress.trim());
		if (Logger.isDebugEnabled()) {
			Logger.debug("%s", url);
		}
		return gson.fromJson(new InputStreamReader(url.openStream()), Location.class); // NOPMD
	}
}
