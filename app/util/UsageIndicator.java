package util;

import models.BookedFlatrate;
import models.LaundryItem;

/**
 * Indicates the current Usages
 * @author Sebastian
 * 
 */
public class UsageIndicator {

	private final LaundryItem laundryItem;
	private final int itemsUsed;
	private final int capacity;
	private final double degreeOfCapacity;

	public UsageIndicator(LaundryItem laundryItem, long itemsUsed, int capacity) {
		super();
		this.laundryItem = laundryItem;
		this.itemsUsed = (new Long(itemsUsed)).intValue();
		this.capacity = capacity;
		this.degreeOfCapacity = 1.0 * itemsUsed / capacity;
	}
	

	/**
	 * @return the laundryItem
	 */
	public LaundryItem getLaundryItem() {
		return laundryItem;
	}

	/**
	 * @return the itemsUsed
	 */
	public int getItemsUsed() {
		return itemsUsed;
	}

	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * @return the degreeOfCapacity
	 */
	public double getDegreeOfCapacity() {
		return degreeOfCapacity;
	}

	@Override
	public String toString() {
		return "UsageIndicator [laundryItem=" + laundryItem.name + ", itemsUsed=" + itemsUsed + ", capacity=" + capacity + ", degreeOfCapacity=" + degreeOfCapacity + "]";
	}
	
	
}
