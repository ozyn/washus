package util;

import java.util.Calendar;

public class FormatUtil {
	
	public static int getMonth(Calendar cal){
		return cal.get(Calendar.MONTH);
	}
	
	public static int getMonth(){
		return Calendar.getInstance().get(Calendar.MONTH);
	}
	
	public static int getYear(Calendar cal){
		return cal.get(Calendar.YEAR);
	}
	
	public static int Year(){
		return Calendar.getInstance().get(Calendar.YEAR);
	}

}
