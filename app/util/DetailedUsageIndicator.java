package util;

import java.util.Calendar;
import java.util.Date;

import play.Logger;
import models.Laundry;
import models.LaundryItem;
import models.User;


/**
 * Indicates the usages more detailed (for usage overview)
 * @author Sebastian
 *
 */
public class DetailedUsageIndicator {

	public int month;
	public int year;
	public int day;
	public Laundry laundry;
	public LaundryItem laundryItem;
	public long count;

	public DetailedUsageIndicator(int month, int year, Date date, Laundry laundry, LaundryItem laundryItem, long count) {
		super();
		this.month = month;
		this.year = year;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		this.day = cal.get(Calendar.DAY_OF_MONTH);
		this.laundry = laundry;
		this.laundryItem = laundryItem;
		this.count = count;
	}
	
	public DetailedUsageIndicator(int month, int year, int date, Laundry laundry, LaundryItem laundryItem, long count) {
		super();
		this.day = date;
		this.laundry = laundry;
		this.laundryItem = laundryItem;
		this.count = count;
	}
	
	

	@Override
	public String toString() {
		return "DetailedUsageIndicator [month=" + month + ", year=" + year + ", day=" + day + ", count=" + count + ", laundry=" + laundry.getName() + ", laundryItem=" + laundryItem.name + "]";
		}

}
