package util.hash;

/**
 * Classes extending this interface can be used for hashing passwords for being
 * saved in the db
 * 
 * @author Michael Ostner
 * 
 */
public interface PasswordHasher {
	public String hash(String plain);
}
