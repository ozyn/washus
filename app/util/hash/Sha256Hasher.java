package util.hash;

import java.nio.charset.Charset;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * An implementation of the {@link PasswordHasher} using the Sha265 Algorithm
 * 
 * @author Michael Ostner
 * 
 */
public class Sha256Hasher implements PasswordHasher {

	private static final HashFunction HASH_ALGORITHM = Hashing.sha256();
	private static final String SALT = "mylittleSecretWhichNobodyKnows";
	private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

	@Override
	public String hash(String plain) {
		return HASH_ALGORITHM.hashString(plain + SALT, DEFAULT_CHARSET).toString();
	}
}
