package jobs.async;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import javax.inject.Inject;

import play.Logger;
import play.jobs.Job;
import util.ip.PrivatePublicIpDistinguisher;
import util.ip.PublicIpResolver;
import util.location.Location;
import util.location.LocationFinder;

/**
 * Helps to do the location finding asynchronously to speed up our application
 * 
 * @author Michael Ostner
 * 
 */
public class FindLocationJob extends Job<Location> implements Serializable {
	private final String remoteAddress;

	@Inject
	private static LocationFinder locationFinder;
	@Inject
	private static PrivatePublicIpDistinguisher ppid;
	@Inject
	private static PublicIpResolver ipResolver;

	public FindLocationJob(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	@Override
	public Location doJobWithResult() throws Exception {
		Location location;

		if (Logger.isDebugEnabled())
			Logger.debug("Searching location for %s", getIpAddress());
		
		try {
			location = locationFinder.getLocation(getIpAddress());
			if (Logger.isDebugEnabled())
				Logger.debug("found location: %s", location);
		} catch (IOException e) {
			location = new Location(null, null, "munich", "bavaria", "DE", null, null);
			if (Logger.isEnabledFor("ERROR"))
				Logger.error(e, "could not load location because of IOException");
		}
		
		return location;
	}

	private String getIpAddress() throws IOException {
		if (ppid.isLocaleIp(remoteAddress)) {
			return ipResolver.findPublicIp();
		} else {
			return remoteAddress;
		}
	}
}
