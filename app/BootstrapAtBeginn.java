import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.BookedFlatrate;
import models.Condition;
import models.Flatrate;
import models.FlatrateItem;
import models.Hotel;
import models.Laundry;
import models.LaundryItem;
import models.Role;
import models.Usage;
import models.User;
import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;
import util.ActionUtil;
import util.FormatUtil;

/**
 * Startup
 * 
 * @author Sebastian
 */
@OnApplicationStart
public class BootstrapAtBeginn extends Job {

	public void doJob() {
		 try {
		if (Flatrate.count() == 0) { // DB is empty at start,-> no
										// duplicated files

			if (Flatrate.count() == 0) {
				Fixtures.loadModels("flatrates.yml");
			}
			if (User.count() == 0) {
				Fixtures.loadModels("users.yml");
			}
			if (Hotel.count() == 0) {
				Fixtures.loadModels("hotels.yml");
			}
			if (Laundry.count() == 0) {
				Fixtures.loadModels("laundries.yml");
			}
			if (LaundryItem.count() == 0) {
				Fixtures.loadModels("laundryItems.yml");
			}
			if (Role.count() == 0) {
				Fixtures.loadModels("roles.yml");
			}
			
			
			User lUser = User.findByEmail("ulla@waschfrau.de");
			
			Laundry lu = Laundry.findById(1l);
			lUser.setLaundry(lu);
			lu.setUser(lUser);
			lUser.save();
			lu.save();

			LaundryItem hemd = LaundryItem.find("byName", "Shirt").first();
			LaundryItem hose = LaundryItem.find("byName", "Trousers").first();
			LaundryItem sakko = LaundryItem.find("byName", "Jacket").first();

			List<Flatrate> flatrates = Flatrate.findAll();

			// Flatrate 0:
			Flatrate f = flatrates.get(0);
			addFlatrateItem(hemd, f, 20);

			// Flatrate 1:
			f = flatrates.get(1);
			addFlatrateItem(hemd, f, 20);
			addFlatrateItem(hose, f, 4);
			addFlatrateItem(sakko, f, 4);

			// Flatrate 2:
			f = flatrates.get(2);
			addFlatrateItem(hemd, f, 24);
			addFlatrateItem(hose, f, 8);
			addFlatrateItem(sakko, f, 8);

			// Flatrate 3:
			f = flatrates.get(3);
			addFlatrateItem(hemd, f, 32);
			addFlatrateItem(hose, f, 12);
			addFlatrateItem(sakko, f, 12);

			// Add user1 flatrate1
			User user = (User) User.findAll().get(0);
			Calendar cal = Calendar.getInstance();

			Flatrate flatrate = (Flatrate) Flatrate.findAll().get(1);

			// Laundry incl. condition:
			Laundry l = (Laundry) Laundry.findAll().get(0);
			l.addCondition(new Condition(l, hemd, 1.2));
			l.addCondition(new Condition(l, sakko, 5));
			l.addCondition(new Condition(l, hose, 4));
			l.save(); //CascadeType.All

			// Laundry incl. condition:
			l = (Laundry) Laundry.findAll().get(1);
			l.addCondition(new Condition(l, hemd, 1.5));
			l.addCondition(new Condition(l, sakko, 4.5));
			l.addCondition(new Condition(l, hose, 3.8));

			l.save(); //CascadeType.All

			List<Flatrate> flatrateList = Flatrate.findAll();
			List<Condition> conditionList = Condition.findAll();
			
			

			// ADDING BOOKEDFLATRES
			// next Month
			Calendar calNext = Calendar.getInstance();
			calNext.add(Calendar.MONTH, 1);
			user.addBookedFlatrate(new BookedFlatrate(user, flatrate, calNext.getTime(), FormatUtil.getMonth(calNext), FormatUtil.getYear(calNext)));
			// current Month
			cal = Calendar.getInstance(); // only for safety reasons
			user.addBookedFlatrate(new BookedFlatrate(user, flatrate, cal.getTime(), FormatUtil.getMonth(cal), FormatUtil.getYear(cal)));

			// x months ago
			int months = 7;
			for (int i = 0; i < months; i++) {
				// System.out.println("i="+i);
				cal.add(Calendar.MONTH, -1);
				Flatrate rdmFlatrate = (Flatrate) getRdmObject(flatrateList);

				user.addBookedFlatrate(new BookedFlatrate(user, rdmFlatrate, cal.getTime(), FormatUtil.getMonth(cal), FormatUtil.getYear(cal)));
			}

			// save it:
			user.save();
			for (BookedFlatrate bf : user.getBookedFlatrates()) {
				bf.save();
			}

			// Usages:
			for (BookedFlatrate bf : user.getBookedFlatrates()) {
				// current month:
				if (bf.getBookedMonth() == Calendar.getInstance().get(Calendar.MONTH)) {
					Map<Condition, Integer> newUsages = new LinkedHashMap<>();
					
					newUsages.put((Condition) Condition.findById(1l), 2);
					newUsages.put((Condition) Condition.findById(2l), 2);

					// save it
					ActionUtil.addUsages(newUsages, bf, null, Calendar.getInstance().getTime());
				} else if (bf.getBookedMonth() == calNext.get(Calendar.MONTH)) {
					// no usages for next month of course :)
				} else {
					// rdm. usages for last months

					for (int d = 0; d < Math.random() * 5 + 1; d++) {
						// set day
						cal = Calendar.getInstance();
						cal.set(Calendar.MONTH, bf.getBookedMonth());
						cal.set(Calendar.YEAR, bf.getBookedYear());
						int day = (int) (Math.random() * 28 + 1);
						cal.set(Calendar.DAY_OF_MONTH, day);
						Map<Condition, Integer> newUsages = new LinkedHashMap<>();

						// create items between 1 and 4 of given day
						for (int i = 1; i <= 4; i++) {
							List<FlatrateItem> fiList = new LinkedList<>();
							fiList.addAll(bf.getFlatrate().flatrateItems);

							FlatrateItem flatrateItem = getRdmObject(fiList);
							// int count = (int) (Math.random() *
							// flatrateItem.capacity + 1);
							int count = (int) (Math.random() * ActionUtil.getCapacityLeft(bf).get(flatrateItem.laundryItem)) + 1;
							Condition cond = null;
							
							
							//get rdm condition which fit
							int rdm = (int) (Math.random()*conditionList.size());
							Condition c = conditionList.get(rdm);
							conditionList.remove(c);
							for(int ii = 0 ; ii<5; ii++){
								c = conditionList.set((int) (Math.random()*conditionList.size()), c);
							}
							conditionList.add(c);
							for (Condition co : conditionList) {
								if (co.getLaundryItem() == flatrateItem.laundryItem) {
									cond = co;
									break;
								}
							}
							
							//add usage
							if (cond != null) {
								newUsages.put(cond, count);
							}

						}
						bf = ActionUtil.addUsages(newUsages, bf, null, cal.getTime());

					}

				}
			}


			// add roles
			Role adminRole = Role.findByName("ADMIN");
			Role laundryRole = Role.findByName("LAUNDRY");
			Role userRole = Role.findByName("USER");

			setUserRole("admin@wash.us", adminRole);
			setUserRole("ulla@waschfrau.de", laundryRole);
			setUserRole("peter@lustig.de", userRole);
			setUserRole("hans@gluecklich.de", userRole);

		}

		} catch (Throwable e) {
			// :(
			Logger.error(e.getMessage());
		}
	}

	private void setUserRole(String email, Role role) {
		User u = User.findByEmail(email);
		u.setRole(role);
		u.save();
	}

	private Flatrate addFlatrateItem(LaundryItem item, Flatrate flatrate, int count) {
		if (flatrate.flatrateItems == null) {
			flatrate.flatrateItems = new LinkedHashSet<>();
		}
		FlatrateItem fi = new FlatrateItem(flatrate, item, count);
		fi.save();
		flatrate.flatrateItems.add(fi);
		flatrate.save();

		return flatrate;
	}

	private <T> T getRdmObject(List<T> list) {
		int size = list.size();
		int index = (int) (Math.random() * size);
		return list.get(index);
	}

}
