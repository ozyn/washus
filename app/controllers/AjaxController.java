package controllers;

import java.util.List;

import jobs.async.FindLocationJob;
import models.Laundry;
import play.cache.Cache;
import play.libs.F.Promise;
import play.mvc.Controller;
import util.location.Location;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AjaxController extends Controller {

	public static void findLocation() {
		final String key = session.getId() + "_location";

		Location location = (Location) Cache.get(key);
		if (location == null) {
			Promise<Location> promise = new FindLocationJob(request.remoteAddress).now();
			location = await(promise);
			Cache.add(key, location, "12h");
		}

		renderJSON(location);
	}

	public static void getLaundries() {
		List<Laundry> laundries = Laundry.all().fetch();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

		renderJSON(gson.toJson(laundries));
	}

}
