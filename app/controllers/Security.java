package controllers;

import play.Logger;
import play.cache.Cache;
import play.mvc.Scope.Session;
import util.ActionUtil;
import models.*;

/**
 * The custom security class to provide login and role functions
 * 
 * @author Michael Ostner
 * 
 */
public class Security extends Secure.Security {

	static boolean authenticate(String username, String password) {
		return User.connect(username, password) != null;
	}

	static boolean check(String profile) {
		return ActionUtil.getConnectedUser(session).hasRole(profile);
	}

	static void onAuthenticated() {
		session.put("username", User.findByEmail(connected()).getId());
		
		final Role role = ActionUtil.getConnectedUser(session).getRole();
		final String roleName = role.name.toLowerCase();
		
		session.put("rolename", role.name);
		
		if(Logger.isDebugEnabled())
			Logger.debug("User is logged in. His role is '%S'", roleName);
		
		switch (roleName) {
		case "user":
		case "admin":
			PrivateController.index();
			break;
		case "laundry":
			LaundryController.index();
			break;
		default:
			forbidden();
		}
	}

	static void onDisconnected() {
		PublicController.index();
	}
}