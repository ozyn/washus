package controllers;

import java.util.List;

import javax.inject.Inject;

import models.Flatrate;
import models.Payment;
import models.Role;
import models.User;
import play.Logger;
import play.cache.Cache;
import play.data.validation.Valid;
import play.mvc.Controller;
import util.hash.PasswordHasher;
import util.location.Location;

/**
 * This class contains all methods that routs to the public pages or handles all
 * public stuff
 * 
 * @author Michael Ostner
 * 
 */
public class PublicController extends Controller {

	public static void index() {
		Location location = (Location) Cache.get(session.getId() + "_location");

		String address, region;
		if (location != null) {
			address = location.getCity();
			region = location.getCountry();
		} else {
			address = "New York";
			region = "US";
		}

		// Get Flatrates
		List<Flatrate> flatrates = Flatrate.findAll();

		render(address, region, flatrates);
	}

	public static void impressum() {
		render();
	}

	public static void registerForm(@Valid User user /* , @Valid Payment payment */) {
		required("user.password");
		required("user.terms_conditions");
		required("user.privacy_policy");
		validateRepeatedPassword("user.password", "user.repeatPassword");

		// Logger.debug("%s", payment);

		if (validation.hasErrors()) {
			params.remove("user.password");
			params.remove("user.repeatedPassword");
			
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request

			register();
		} else {
			user.setKey(User.getNewKey());
			user.setRole(Role.findByName("USER"));
			user.save();
			// payment.save();
			index();
		}
	}

	private static void required(final String key) {
		validation.required(params.get(key)).key(key);
	}

	private static void validateRepeatedPassword(String originalPasswordKey, String repeatedPasswordKey) {
		final String originalPassword = params.get(originalPasswordKey);
		final String repeatedPassword = params.get(repeatedPasswordKey);
		validation.equals(originalPassword, repeatedPassword).key("user.repeatPassword").message("The repeated password must be the same as the origial password");
	}

	public static void register() {
		render();
	}
}