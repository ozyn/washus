package controllers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.BookedFlatrate;
import models.Condition;
import models.Laundry;
import models.User;
import play.Logger;
import play.cache.Cache;
import play.mvc.Controller;
import play.mvc.With;
import util.ActionUtil;
import util.LaundryBookItems;
import util.LaundryUsageIndicator;
import util.UsageIndicator;

/**
 * Controller for users having the laundry role
 * 
 * @author Michael Ostner
 * 
 */
@With(Secure.class)
@Check("LAUNDRY")
public class LaundryController extends Controller {

	public static void index() {
		// get User:
		User user = ActionUtil.getConnectedUser(session);
		
		Laundry laundry = user.getLaundry();

		List<LaundryUsageIndicator> laundryUsageIndicators = ActionUtil.getLaundryUsageIndicator(laundry);
		Map<Long, List<LaundryUsageIndicator>> laundryUsageIndicatorsGrouped = ActionUtil.getLaundryUsageIndicator(laundry, 5);
		
		render(laundryUsageIndicators, laundry, laundryUsageIndicatorsGrouped, user);
	}

	public static void key() {
		render();
	}

	public static void handIn(String key) {
		User u = (User) User.find("byKey", key).first();
		BookedFlatrate bookedFlatrate = ActionUtil.getBookedFlatrateForCurrentMonth(u);

		if (u == null || bookedFlatrate == null) {
			validation.required(null).message("No flatrate available for given key!");
			params.flash();
			validation.keep();

			key();
		}

		Cache.set(session.getId() + "_key", key);
		Cache.set(session.getId() + "_bf", bookedFlatrate.id);

		List<LaundryBookItems> laundryBookItems = ActionUtil.getLaundryBookItems(key, bookedFlatrate);

		List<UsageIndicator> usageIndicators = new LinkedList<>();
		usageIndicators = ActionUtil.getUsageIndicators(bookedFlatrate);

		render(bookedFlatrate, laundryBookItems, usageIndicators);
	}

	public static void summary(Map<String, Integer> itemsToBook) {	
		//if redirect from success
		if(itemsToBook == null){
			itemsToBook = (Map<String, Integer>) Cache.get(session.getId() + "_itemsToBook");
		}
		
		Object bfId = Cache.get(session.getId() + "_bf");
		Object uKey = Cache.get(session.getId() + "_key");
		
		if(bfId == null || uKey == null || itemsToBook == null){
			//Cache Error -> go back to start
			key();
		}
		
		BookedFlatrate bookedFlatrate = BookedFlatrate.findById(bfId);
		User u = (User) User.find("byKey", uKey).first();

		if (bookedFlatrate == null || u == null) {
			key();
		}

		// save selected items + values in cache
		Cache.set(session.getId() + "_itemsToBook", itemsToBook);

		render(itemsToBook);
	}
	
	/**
	 * dummyMethod for false PIN
	 */
	public static void summary(){
		Map<String, Integer> itemsToBook = (Map<String, Integer>) Cache.get(session.getId() + "_itemsToBook");
		if(itemsToBook == null){
			key();
		}
		render(itemsToBook);
	}

	public static void success(String securecode) {
		Map<String, Integer> itemsToBook = (Map<String, Integer>) Cache.get(session.getId() + "_itemsToBook");
		
		Object bfId = Cache.get(session.getId() + "_bf");
		Object uKey = Cache.get(session.getId() + "_key");
		
		if(bfId == null || uKey == null || itemsToBook == null || securecode == null){
			//Cache Error -> go back to start
			key();
		}
		
		BookedFlatrate bookedFlatrate = BookedFlatrate.findById(bfId);
		User u = (User) User.find("byKey", uKey).first();
		User laundryUser = ActionUtil.getConnectedUser(session);

		if (bookedFlatrate == null || u == null) {
			//Error cause of false parameters
			key();
		}
		
		Map<String, Integer> itb = new HashMap<>();
		for(String s: itemsToBook.keySet()){
			itb.put(s, itemsToBook.get(s));
		}

		if (!u.getPin().equals(securecode)) {
			validation.required(null).message("Wrong PIN");
			params.flash();
			validation.keep();
			LaundryController.summary();
		}

		Laundry l = laundryUser.getLaundry();
		Map<Condition, Integer> newUsages = new HashMap<>();

		for (Condition c : l.getConditions()) {
			for (String liName : itemsToBook.keySet()) {
				if (c.getLaundryItem().name.equals(liName)) {
					Integer count = itemsToBook.get(liName);
					if(count >= 0){
						newUsages.put(c, itemsToBook.get(liName));
					}
				}
			}
		}


		ActionUtil.addUsages(newUsages, bookedFlatrate, null, Calendar.getInstance().getTime());
		render();
	}

}
