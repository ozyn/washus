package controllers;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;

import models.BookedFlatrate;
import models.Flatrate;
import models.Laundry;
import models.LaundryItem;
import models.User;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import play.Logger;
import play.data.validation.Valid;
import play.data.validation.Validation.ValidationResult;
import play.mvc.Controller;
import play.mvc.With;
import util.ActionUtil;
import util.DetailedUsageIndicator;
import util.UsageIndicator;

@With(Secure.class)
public class PrivateController extends Controller {

	public static void index() {

		// get User:
		User user = ActionUtil.getConnectedUser(session);

		/*
		 * Get Flatrates
		 */
		List<Flatrate> flatrates = Flatrate.findAll();

		// if (Logger.isDebugEnabled())
		// Logger.debug("All current usages: %s", Usage.findAll());

		// Current used Flatrate
		List<UsageIndicator> usageIndicators = new LinkedList<>();
		BookedFlatrate bookedFlatrate = ActionUtil.getBookedFlatrateForCurrentMonth(user);
		usageIndicators = ActionUtil.getUsageIndicators(bookedFlatrate);

		/*
		 * Flatrate for next Month:
		 */
		BookedFlatrate bookedFlatrateForNextMonth = ActionUtil.getBookedFlatrateForNextMonth(user);

		// /*
		// * UsageMap:
		// */
		// List<DetailedUsageIndicator> detailedUsageIndicators =
		// ActionUtil.getDetailedUsageIndicators(user);

		Map<Long, Map<String, Map<Laundry, Map<LaundryItem, DetailedUsageIndicator>>>> uMap = ActionUtil.getDetailedUsageIndicatorsSorted(user);
		
		List<DetailedUsageIndicator> duiList = ActionUtil.getDetailedUsageIndicators(bookedFlatrate);
		render(usageIndicators, bookedFlatrate, flatrates, bookedFlatrateForNextMonth, uMap,duiList, user);
	}

	/**
	 * changes the flatrate of next Month
	 * 
	 * @param flatrateId
	 *            change flatrate of next month to Flatrate with given id, if id
	 *            < 1 (e.g. -1) delete the bookedFlatrate
	 */
	public static void changeFlatrate(long flatrateId) {

		// get User:
		User user = ActionUtil.getConnectedUser(session);

		Flatrate chosenFlatrate = null;

		if (flatrateId >= 1) {
			chosenFlatrate = Flatrate.findById(flatrateId);
		}

		ActionUtil.changeBookedFlatrateForNextMonth(user, chosenFlatrate);

		index();
	}

	public static void upgradeFlatrate(long flatrateId) {
		try {
			User user = ActionUtil.getConnectedUser(session);
			Flatrate chosenFlatrate = Flatrate.findById(flatrateId);

			// Tests if upgrade can happen, and update
			boolean upgradeSuccessfull = ActionUtil.upgradeBookedFlatrate(chosenFlatrate, user);

			if (!upgradeSuccessfull) {
				validation.required(null).message("You can only upgrade your Flatrate. Please contact us for more Informations.");
				params.flash();
				validation.keep();
			}
			index();

		} catch (Exception e) {
			Logger.debug("ERROR in Upgrade Process", e);

			validation.required(null).message("You can only upgrade your Flatrate. Please contact us for more Informations.");
			params.flash();
			validation.keep();
			index();
		}

	}

	/**
	 * create QR Code for Payment
	 * 
	 */
	public static void getPayment() {
		// get User:
		User user = ActionUtil.getConnectedUser(session);

		// only for testing purpose -> normally real route will be given instead
		// of localhost
		String output = "http://localhost:9000/laundry/handIn?key=" + user.getKey();
		File qr = QRCode.from(output).to(ImageType.PNG).file();

		response.setContentTypeIfNotSet("PNG");

		if (qr != null) {
			renderBinary(qr);
		} else {
			validation.required(qr).message("An error occurred. Please contact administration");
			params.flash();
			validation.keep();
			index();
		}
	}

	/**
	 * goto preferences.html
	 */
	public static void preferences() {
		User user = ActionUtil.getConnectedUser(session);

		Preconditions.checkNotNull(user, "user must not be null");

		insertIfNotNull("user.firstname", user.getFirstname());
		insertIfNotNull("user.lastname", user.getLastname());
		insertIfNotNull("user.email", user.getEmail());
		insertIfNotNull("user.street", user.getStreet());
		insertIfNotNull("user.number", user.getNumber());
		insertIfNotNull("user.zipCode", user.getZipCode());
		insertIfNotNull("user.city", user.getCity());
		insertIfNotNull("user.pin", user.getPin());
		insertIfNotNull("user.gender", user.getPin());

		render(user);
	}

	private static void insertIfNotNull(String parameterName, String value) {
		if (value != null)
			flash(parameterName, value);
		// else
		// flash(parameterName, "");
	}

	/**
	 * update preferences of a user
	 */
	public static void updatePreferences(@Valid User user) {
		final String password = params.get("user.password");
		final String repeatedPassword = params.get("user.repeatPassword");
		final boolean replacePW = handlePasswordUpdate(user, password, repeatedPassword);

		if (validation.hasErrors()) {
			validation.keep();
		} else {
			final User currentUser = ActionUtil.getConnectedUser(session);

			if (replacePW) {
				Logger.debug("new PW: %s", password);
				currentUser.setPassword(password);
			}

			currentUser.updatePreferences(user);
			currentUser.save();

			flash.success("The data were changed successfully.");
		}

		params.remove("user.password");
		params.remove("user.repeatPassword");

		params.flash();

		// goto preferences view
		preferences();
	}

	/**
	 * @param user
	 * @param password
	 * @param repeatedPassword
	 * @return true if a password should be replaced by <code>password</code>
	 */
	private static boolean handlePasswordUpdate(User user, String password, String repeatedPassword) {
		if (password == null || password.trim().isEmpty()) {
			return false;
		}

		ValidationResult validationResult = validation.equals(password, repeatedPassword);
		validationResult = validationResult.key("user.repeatPassword");
		validationResult = validationResult.message("The password must equal the repeated password.");

		return password.equals(repeatedPassword);
	}
}