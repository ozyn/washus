import play.jobs.Job;
import play.jobs.OnApplicationStop;
import play.test.Fixtures;

@OnApplicationStop
public class BootstrapAtEnd extends Job {
	@Override
	public void doJob() throws Exception {
		Fixtures.deleteDatabase();
	}
}
