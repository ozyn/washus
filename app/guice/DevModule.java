package guice;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;

import play.modules.guice.GuiceSupport;
import util.hash.PasswordHasher;
import util.hash.Sha256Hasher;
import util.ip.PrivatePublicIpDistinguisher;
import util.ip.PrivatePublicIpDistinguisherImpl;
import util.ip.PublicIpResolver;
import util.ip.publicIpResolverImpl.IdentMeIPv4;
import util.ip.publicIpResolverImpl.IfConfigMe;
import util.location.LocationFinder;
import util.location.byIPAddress.IPInfo;

/**
 * The Module used for guice during developing
 * @author Michael Ostner
 *
 */
public class DevModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(PasswordHasher.class).to(Sha256Hasher.class);
		bind(LocationFinder.class).to(IPInfo.class);
		bind(PrivatePublicIpDistinguisher.class).to(PrivatePublicIpDistinguisherImpl.class);
		bind(PublicIpResolver.class).to(IdentMeIPv4.class);
	}

}
