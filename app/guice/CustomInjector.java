package guice;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import play.modules.guice.GuiceSupport;

/**
 * Our custom injector for the guice module
 * 
 * @author Michael Ostner
 * 
 */
public class CustomInjector extends GuiceSupport {

	final private AbstractModule[] modules = new AbstractModule[] { new DevModule() };
	final private Injector injector = Guice.createInjector(modules);

	@Override
	protected Injector configure() {
		return injector;
	}

	public Injector getInjector() {
		return injector;
	}
}
